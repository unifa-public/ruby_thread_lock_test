# frozen_string_literal: true

module Benchmarks
  class ImageResizeCommand
    def initialize(input_path_format, output_path_format, width:, height:)
      @input_path_format = input_path_format
      @output_path_format = output_path_format
      @resize_option = "#{width.to_i}x#{height.to_i}"
    end

    def call(thread_num)
      `convert -resize #{@resize_option} #{format(@input_path_format, thread_num)} #{format(@output_path_format, thread_num)}`
    end
  end
end
