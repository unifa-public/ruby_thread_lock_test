# frozen_string_literal: true

require 'zip'

module Benchmarks
  class ZipRubyzip
    def initialize(input_files, output_dir)
      @input_files = input_files
      @output_dir = output_dir
    end

    def call(thread_num)
      output_path = "#{@output_dir}/#{thread_num}.zip"
      File.delete(output_path) if File.exist?(output_path)

      Zip::File.open(output_path, Zip::File::CREATE) do |zip|
        @input_files.each do |input_file|
          zip.add(input_file, input_file)
        end
      end
    end
  end
end
