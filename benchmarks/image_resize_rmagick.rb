# frozen_string_literal: true

require 'rmagick'

module Benchmarks
  class ImageResizeRMagick
    include ::Magick

    def initialize(input_path_format, output_path_format, width: ,height:)
      @input_path_format = input_path_format
      @output_path_format = output_path_format
      @width = width
      @height = height
    end

    def call(thread_num)
      image = Image.read(format(@input_path_format, thread_num))[0]
      image.resize_to_fit!(@width, @height)
      image.write(format(@output_path_format, thread_num))
    end
  end
end
