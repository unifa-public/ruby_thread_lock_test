# frozen_string_literal: true

require 'prime'

module Benchmarks
  class CalcPrime
    def initialize(take_num)
      @take_num = take_num
      Prime.first(take_num) # Warm up Prime singleton instance
    end

    def call(_thread_num)
      Prime.take(@take_num)
    end
  end
end
