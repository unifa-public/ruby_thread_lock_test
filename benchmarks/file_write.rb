# frozen_string_literal: true

module Benchmarks
  class FileWrite
    # 1M bytes string
    BASE_STR = '0' * 1024 * 1024

    def initialize(path_format, mega_bytes)
      @path_format = path_format
      @mega_bytes = mega_bytes
    end

    def call(thread_num)
      File.open(format(@path_format, thread_num), 'w') do |f|
        @mega_bytes.times { f.print BASE_STR }
      end
    end
  end
end
