# frozen_string_literal: true

require 'mail'

module Benchmarks
  class SmtpSendMail
    FROM = 'from@example.com'
    SUBJECT = 'test'
    BODY = 'This is a test mail.'

    def initialize(to_addr, smtp_options)
      @base_mail = Mail.new do
        from 'from@example.com'
        to to_addr
        subject 'test'
        body 'This is a test mail'
      end
      @base_mail.delivery_method :smtp, smtp_options
    end

    def call(_thread_num)
      @base_mail.dup.deliver!
    end
  end
end
