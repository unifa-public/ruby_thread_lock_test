# frozen_string_literal: true

require 'pg'

module Benchmarks
  class DbSelect
    def initialize(**connect_options)
      @connect_options = connect_options
    end

    def call(_thread_num)
      conn = PG.connect(@connect_options)
      begin
        conn.exec("SELECT pg_sleep(1);")
      ensure
        conn.close
      end
    end
  end
end
