# frozen_string_literal: true

require 'bundler/setup'

require 'uri'
require 'net/http'

require 'benchmark'
require 'parallel'

require_relative 'benchmarks/smtp_send_mail'
require_relative 'benchmarks/file_write'
require_relative 'benchmarks/db_select'
require_relative 'benchmarks/calc_prime'
require_relative 'benchmarks/image_resize_command'
require_relative 'benchmarks/image_resize_rmagick'
require_relative 'benchmarks/zip_rubyzip'

THREAD_NUM = 4
BENCHMARKS = [
  { name: 'sleep_ruby', count: 3, proc: proc { |_| sleep 1 } },
  { name: 'sleep_command', count: 3, proc: proc { |_| `sleep 1` } },

  {
    name: 'http_get',
    count: 3,
    proc: proc { Net::HTTP.get(URI.parse('https://bitbucket.org/unifa-public/ruby_thread_lock_test')) },
  },
  {
    name: 'smpt_send_mail',
    count: 1,
    proc: Benchmarks::SmtpSendMail.new(ENV['SMTP_TO_ADDR'],
                                       address: 'smtp.gmail.com',
                                       port: 587, domain: 'gmail.com',
                                       authentication: 'plain',
                                       user_name: ENV['SMTP_USER_NAME'],
                                       password: ENV['SMTP_PASSWORD'],
                                       enable_starttls_auto: true),
  },
  {
    name: 'db_select',
    count: 3,
    proc: Benchmarks::DbSelect.new(host: 'localhost', user: 'postgres'),
  },

  { name: 'file_read', count: 30, proc: proc { |num| IO.read("data/text/#{num}.txt") } },
  {
    name: 'file_write',
    count: 30,
    proc: Benchmarks::FileWrite.new('tmp/%d.txt', 100),
  },

  { name: 'calc_prime', count: 3, proc: Benchmarks::CalcPrime.new(5_000_000) },

  {
    name: 'image_resize_convert',
    count: 10,
    proc: Benchmarks::ImageResizeCommand.new('data/images/%d.jpg', 'tmp/%d.jpg',
                                             width: 256, height: 182),
  },
  {
    name: 'image_resize_rmagick',
    count: 10,
    proc: Benchmarks::ImageResizeRMagick.new('data/images/%d.jpg', 'tmp/%d.jpg',
                                             width: 256, height: 182),
  },

  { name: 'zip_command', count: 2, proc: proc { |num| `zip -r tmp/#{num}.zip data` } },
  {
    name: 'zip_rubyzip',
    count: 2,
    proc: Benchmarks::ZipRubyzip.new(Dir['data/**/*'], 'tmp'),
  },
]

BENCHMARKS.each do |benchmark|
  name, count, proc = benchmark.values_at(:name, :count, :proc)
  puts "Start: #{name}"

  Benchmark.bm(15) do |r|
    r.report "Single thread" do
      Parallel.in_threads(count: 1) { |num| (count * THREAD_NUM).times { proc.call(num + 1) } }
    end
    r.report "Multi thread" do
      Parallel.in_threads(count: THREAD_NUM) { |num| count.times { proc.call(num + 1) } }
    end
    r.report "Multi process" do
      Parallel.in_processes(count: THREAD_NUM) { |num| count.times { proc.call(num + 1) } }
    end
  end

  puts "End: #{name}"
end
