# Ruby thread lock test

RubyのThreadがロックされてしまうのは、どういう処理なのかをチェックするプロジェクトです。

## 事前準備

* Rubyが使用可能なこと
* 使用しているRubyのバージョンで `bundler` が使用可能なこと
* `localhost` のポート番号 `5432` でPostgreSQLが動作していること
* GmailでSMTP送信可能なアカウントがあること

## 実行方法

    $ git clone git@bitbucket.org:unifa-public/ruby_thread_lock_test.git
    $ cd ruby_thead_lock_test
    $ bundle install
    $ export SMTP_TO_ADDR=メール送信の送信先アドレス
    $ export SMTP_USER_NAME=Gmailアカウントのメアド
    $ export SMTP_PASSWORD=Gmailアカウントのパスワード
    $ ruby start.rb
